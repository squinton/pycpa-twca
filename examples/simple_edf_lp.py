"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
EDF scheduler

"""

from pycpa import analysis
from pycpa import model
from pycpa import options
from pyCPA_TWCA import edf_scheduler
from pyCPA_TWCA import LP_edf as LP
import time


def test():
    # initialize pycpa. (e.g. read command line switches and set up default options)
    options.init_pycpa(True)

    # generate an new system
    s = model.System()

    # add one resource (CPU) to the system
    # and register the static priority preemptive scheduler

    r = s.bind_resource(model.Resource("R", edf_scheduler.EDFScheduler()))

    # create and bind tasks to r
    # here the scheduling_parameter represents the relative deadline

    t1 = r.bind_task(model.Task("T1",  wcet=1, bcet=1, scheduling_parameter=2))
    t2 = r.bind_task(model.Task("T2",  wcet=2, bcet=1, scheduling_parameter=4))
    t3 = r.bind_task(model.Task("T3",  wcet=4, bcet=1, scheduling_parameter=8))
    t4 = r.bind_task(model.Task("T4",  wcet=0.5, bcet=0.5, scheduling_parameter=10))

    # event model
    t1.in_event_model = model.PJdEventModel(P=4)
    t2.in_event_model = model.PJdEventModel(P=5)
    t3.in_event_model = model.EventModel()
    t4.in_event_model = model.EventModel()

    # typical event model
    t1.in_typical_event_model = t1.in_event_model
    t2.in_typical_event_model = t2.in_event_model
    t3.in_typical_event_model = None
    t4.in_typical_event_model = None
    # overload event model
    t1.in_overload_event_model = None
    t2.in_overload_event_model = None
    t3.in_overload_event_model = t3.in_event_model
    t4.in_overload_event_model = t4.in_event_model

    def delta_over_3(q):
        if q < 2 : return 0
        elif q == 2 : return 30
        elif q == 3 : return 100
        elif q == 4 : return 500
        else: return float("inf")

    t3.in_event_model.deltamin_func = delta_over_3
    t3.in_overload_event_model.deltamin_func = delta_over_3

    def delta_over_4(q):
        if q < 2 : return 0
        elif q == 2 : return 45
        elif q == 3 : return 240
        elif q == 4 : return 890
        else: return float("inf")

    t4.in_event_model.deltamin_func = delta_over_4
    t4.in_overload_event_model.deltamin_func = delta_over_4

    #############Analysis Section#############

    task_results = analysis.analyze_system(s)
    for r in sorted(s.resources,key=str):
        for t in sorted(r.tasks,key=str):
            print t.name, task_results[t].wcrt, task_results[t].dmiss

    t_analyzed = t2
    #LP
    sporadic = [t3, t4]
    start = time.time()
    lp = LP.Solve(s, sporadic, t_analyzed, 10)
    print ("dmm (%d) = %d") % (10, lp.get_dmm())
    lp = LP.Solve(s, sporadic, t_analyzed, 100)
    print ("dmm (%d) = %d") % (100, lp.get_dmm())
    lp = LP.Solve(s, sporadic, t_analyzed, 1000)
    print ("dmm (%d) = %d") % (1000, lp.get_dmm())
    end = time.time()
    print"execution time = ", end - start


if __name__ == "__main__":
    test()
