"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.
:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
EDF scheduler

"""

from pycpa import analysis
from pycpa import model
from pycpa import options
from pyCPA_TWCA import edf_scheduler

def test():
    # initialize pycpa. (e.g. read command line switches and set up default options)
    options.init_pycpa(True)

    # generate an new system
    s = model.System()

    # add one resource (CPU) to the system
    # and register the static priority preemptive scheduler

    r = s.bind_resource(model.Resource("R", edf_scheduler.EDFScheduler()))

    # create and bind tasks to r
    # example in M. Spuri
    # here the scheduling_parameter represents the relative deadline
# task_set= {0: [15, 39, 44], 1: [1, 33, 30], 2: [8, 27, 27], 3: [5, 38, 32], 4: [5, 45, 41]}
    t1 = r.bind_task(model.Task("T1", wcet=1, bcet=1, scheduling_parameter=2))
    t2 = r.bind_task(model.Task("T2", wcet=2, bcet=1, scheduling_parameter=4))
    t3 = r.bind_task(model.Task("T3", wcet=4, bcet=1, scheduling_parameter=8))
#     t1 = r.bind_task(model.Task("T1",  wcet=15, bcet=1, scheduling_parameter=39))
#     t2 = r.bind_task(model.Task("T2",  wcet=1, bcet=1, scheduling_parameter=33))
#     t3 = r.bind_task(model.Task("T3",  wcet=8, bcet=1, scheduling_parameter=27))
#     t4 = r.bind_task(model.Task("T4",  wcet=5, bcet=1, scheduling_parameter=38))
#     t5 = r.bind_task(model.Task("T5",  wcet=5, bcet=1, scheduling_parameter=45))

    # event model
    t1.in_event_model = model.PJdEventModel(P=4)
    t2.in_event_model = model.PJdEventModel(P=5)
    t3.in_event_model = model.PJdEventModel(P=30)
    # t1.in_event_model = model.PJdEventModel(P=44)
    # t2.in_event_model = model.PJdEventModel(P=30)
    # t3.in_event_model = model.PJdEventModel(P=27)
    # t4.in_event_model = model.PJdEventModel(P=32)
    # t5.in_event_model = model.PJdEventModel(P=41)

    #############Analysis Section#############
    task_results = analysis.analyze_system(s)
    for r in sorted(s.resources, key=str):
        for t in sorted(r.tasks, key=str):
            print t.name, task_results[t].wcrt, task_results[t].dmiss


if __name__ == "__main__":
    test()
