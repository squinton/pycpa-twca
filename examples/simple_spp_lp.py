"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
SPP scheduler

"""

import time
from pyCPA_TWCA import analysis
from pyCPA_TWCA import model
from pycpa import options
from pyCPA_TWCA import schedulers
from pyCPA_TWCA import LP


def test():
    # initialize pycpa. (e.g. read command line switches and set up default options)
    options.init_pycpa(True)

    # generate an new system
    s = model.System()

    # add one resource (CPU) to the system
    # and register the static priority preemptive scheduler

    r = s.bind_resource(model.Resource("R", schedulers.SPPScheduler()))

    # create and bind tasks to r
    # t0 is a mixed task
    t0p = r.bind_task(model.Task("T0p", wcet=122, bcet=0.5, block = 0, scheduling_parameter=0))
    t0s = r.bind_task(model.Task("T0s", wcet=122, bcet=0.5, block = 0, scheduling_parameter=0))
    # t1 is sporadic
    t1  = r.bind_task(model.Task("T1",  wcet=967, bcet=1,   block = 0, scheduling_parameter=1))

    t2 = r.bind_task(model.Task("T2",   wcet=1524, bcet=1, block = 0, scheduling_parameter=2))
    t3 = r.bind_task(model.Task("T3",   wcet=159, bcet=1, block = 0, scheduling_parameter=3))
    t4 = r.bind_task(model.Task("T4",   wcet=122, bcet=1, block = 0, scheduling_parameter=4))
    # t5 is a mixed task
    t5p = r.bind_task(model.Task("T5p", wcet=51, bcet=1.5, block = 0, scheduling_parameter=5))
    t5s = r.bind_task(model.Task("T5s", wcet=51, bcet=1.5, block = 0, scheduling_parameter=5))

    t6 = r.bind_task(model.Task("T6", wcet=571, bcet=1, block = 0, scheduling_parameter=6))
    t7 = r.bind_task(model.Task("T7", wcet=1080, bcet=1, block = 0, scheduling_parameter=7))
    t8 = r.bind_task(model.Task("T8", wcet=14, bcet=1, block = 0, scheduling_parameter=8))
    # t9 is sporadic
    t9 = r.bind_task(model.Task("T9", wcet=830, bcet=1, block = 0, scheduling_parameter=9))

    # worst-case model
    t0p.in_event_model = model.PJdEventModel(P=1000)
    t0p.D = 300
    t0s.in_event_model = model.EventModel()
    t1.in_event_model  = model.EventModel()
    t2.in_event_model  = model.PJdEventModel(P=8000)
    t2.D = 6400
    t3.in_event_model  = model.PJdEventModel(P=8000)
    t3.D = 2400
    t4.in_event_model  = model.PJdEventModel(P=8000)
    t4.D = 8000
    t5p.in_event_model = model.PJdEventModel(P=2000)
    t5p.D = 600
    t5s.in_event_model = model.EventModel()
    t6.in_event_model  = model.PJdEventModel(P=2000)
    t6.D = 600
    t7.in_event_model  = model.PJdEventModel(P=8000)
    t7.D = 6400
    t8.in_event_model  = model.PJdEventModel(P=8000)
    t8.D = 6800
    t9.in_event_model  = model.EventModel()

    # typical model
    t0p.in_typical_event_model = t0p.in_event_model
    t0s.in_typical_event_model = None
    t1.in_typical_event_model  = None
    t2.in_typical_event_model  = t2.in_event_model
    t3.in_typical_event_model  = t3.in_event_model
    t4.in_typical_event_model  = t4.in_event_model
    t5p.in_typical_event_model = t5p.in_event_model
    t5s.in_typical_event_model = None
    t6.in_typical_event_model  = t6.in_event_model
    t7.in_typical_event_model  = t7.in_event_model
    t8.in_typical_event_model  = t8.in_event_model
    t9.in_typical_event_model  = None


    # overload model
    t0p.in_overload_event_model = None
    t0s.in_overload_event_model = t0s.in_event_model
    t1.in_overload_event_model  = t1.in_event_model
    t2.in_overload_event_model  = None
    t3.in_overload_event_model  = None
    t4.in_overload_event_model  = None
    t5p.in_overload_event_model = None
    t5s.in_overload_event_model = t5s.in_event_model
    t6.in_overload_event_model  = None
    t7.in_overload_event_model  = None
    t8.in_overload_event_model  = None
    t9.in_overload_event_model  = t9.in_event_model

    def delta_over_0(q):
        if q < 2 : return 0
        elif q == 2 : return 20608
        elif q == 3 : return 344436
        elif q == 4 : return 833988
        else: return float("inf")

    t0s.in_event_model.deltamin_func = delta_over_0
    t0s.in_overload_event_model.deltamin_func = delta_over_0

    def delta_over_1(q):
        if q < 2 : return 0
        elif q == 2 : return 16767
        elif q == 3 : return 252149
        elif q == 4 : return 1582320
        else: return float("inf")

    t1.in_event_model.deltamin_func = delta_over_1
    t1.in_overload_event_model.deltamin_func = delta_over_1

    def delta_over_5(q):
        if q < 2 : return 0
        elif q == 2 : return 33017
        elif q == 3 : return 185388
        elif q == 4 : return 464057
        else: return float("inf")

    t5s.in_event_model.deltamin_func = delta_over_5
    t5s.in_overload_event_model.deltamin_func = delta_over_5

    def delta_over_9(q):
        if q < 2 : return 0
        elif q == 2 : return 51614
        elif q == 3 : return 328119
        elif q == 4 : return 1370892
        else: return float("inf")

    t9.in_event_model.deltamin_func = delta_over_9
    t9.in_overload_event_model.deltamin_func = delta_over_9


    #############Analysis Section#############
    # STEP 1: perform the typical worst-case analysis TWCA
    # Two analysis will be performed by analysis.analyze_system()
        # 1. Worst-case analysis
        # 2. Typical worst-case analysis
    task_results = analysis.analyze_system(s)
    for r in sorted(s.resources, key=str):
        for t in sorted(r.tasks, key=str):
            print t.name, task_results[t].wcrt
            # print t.name, task_results[t].twcrt
    # STEP 2: choose the task for which one wants to compute weakly-hard guarantees
    # We choose the lowest priority task
    t_analyzed = t7
    #LP
    sporadic = [t0s, t1, t5s]
    start = time.time()
    lp = LP.Solve(s, sporadic, t_analyzed, 10)
    print lp.get_dmm()
    lp = LP.Solve(s, sporadic, t_analyzed, 100)
    print lp.get_dmm()
    lp = LP.Solve(s, sporadic, t_analyzed, 1000)
    print lp.get_dmm()
    end = time.time()
    print"execution time = ", end - start
    # #ILP
    # print"\nILP"
    # start = time.time()
    # lp = DMM.ILP(s,t_analyzed,[10,100,1000])
    # para = lp.getOutput()
    # print para['dmm']
    # end = time.time()
    # print"execution time = ", end - start

# The results should be as follows:
# T0p 244
# T0s 244
# T1 1333
# T2 2979
# T3 3260
# T4 3382
# T5p 3484
# T5s 3535
# T6 4279
# T7 7489
# T8 7503
# T9 13858
# dmm (10) = 2
# dmm (100) = 3
# dmm (1000) = 4
# execution time =  x.xx (it depends!)


if __name__ == "__main__":
    test()
