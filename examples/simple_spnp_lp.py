"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
SPNP scheduler

"""

import time
from pyCPA_TWCA import analysis
from pyCPA_TWCA import model
from pycpa import options
from pyCPA_TWCA import schedulers
from pyCPA_TWCA import LP


def test():
    # initialize pycpa. (e.g. read command line switches and set up default options)
    options.init_pycpa(True)

    # generate an new system
    s = model.System()

    # add one resource (CPU) to the system
    # and register the static priority preemptive scheduler

    r = s.bind_resource(model.Resource("R", schedulers.SPNPScheduler()))
    # create and bind tasks to r
    t1p= r.bind_task(model.Task("T1p", wcet=3, bcet=0.5, block = 0, scheduling_parameter=1))
    t1s= r.bind_task(model.Task("T1s", wcet=3, bcet=0.5, block = 0, scheduling_parameter=1))
    t2 = r.bind_task(model.Task("T2",  wcet=5, bcet=0.5, block = 0, scheduling_parameter=2))
    t3 = r.bind_task(model.Task("T3",  wcet=3, bcet=1,   block = 0, scheduling_parameter=3))

    t1p.in_event_model = model.PJdEventModel(P=12)
    t1p.D = 12
    t1p.in_typical_event_model = t1p.in_event_model
    t1p.in_overload_event_model = None
    t1s.in_event_model = model.EventModel()
    t1s.in_typical_event_model = None
    t1s.in_overload_event_model = t1s.in_event_model
    t2.in_event_model = model.EventModel()
    t2.in_typical_event_model = None
    t2.in_overload_event_model = t2.in_event_model
    t3.in_event_model = model.PJdEventModel(P=12)
    t3.in_typical_event_model = t3.in_event_model
    t3.in_overload_event_model = None
    t3.D = 12

    def delta_over_0(q):
        if q < 2 : return 0
        elif q == 2 : return 50
        elif q == 3 : return 200
        elif q == 4 : return 700
        else: return float("inf")

    t1s.in_event_model.deltamin_func = delta_over_0
    t1s.in_overload_event_model.deltamin_func = delta_over_0

    def delta_over_1(q):
        if q < 2 : return 0
        elif q == 2 : return 40
        elif q == 3 : return 150
        elif q == 4 : return 600
        else: return float("inf")

    t2.in_event_model.deltamin_func = delta_over_1
    t2.in_overload_event_model.deltamin_func = delta_over_1

    task_results = analysis.analyze_system(s)
    for r in sorted(s.resources, key=str):
        for t in sorted(r.tasks, key=str):
            print t.name, task_results[t].wcrt

    t_analyzed = t3
    sporadic = [t1s, t2]
    start = time.time()
    lp = LP.Solve(s, sporadic, t_analyzed, 10)
    print lp.get_dmm()
    lp = LP.Solve(s, sporadic, t_analyzed, 100)
    print lp.get_dmm()
    lp = LP.Solve(s, sporadic, t_analyzed, 1000)
    print lp.get_dmm()
    end = time.time()
    print"execution time = ", end - start


# The results should be as follows:
# T1p 11
# T1s 11
# T2 14
# T3 14
# dmm (10) = 2
# dmm (100) = 4
# dmm (1000) = 4
# execution time =  x.xx (it depends!)

if __name__ == "__main__":
    test()