"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
Round-Robin scheduler

"""

import time
from pyCPA_TWCA import analysis
from pyCPA_TWCA import model
from pycpa import options
from pyCPA_TWCA import schedulers
from pyCPA_TWCA import LP_wrr as LP


def test():
    # initialize pycpa. (e.g. read command line switches and set up default options)
    options.init_pycpa(True)

    # generate an new system
    s = model.System()

    # add one resource (CPU) to the system
    # and register the static priority preemptive scheduler

    r = s.bind_resource(model.Resource("R", schedulers.RoundRobinScheduler()))
    T1 = r.bind_task(model.Task("T1", wcet=3,  bcet=1, scheduling_parameter=2))
    T2 = r.bind_task(model.Task("T2", wcet=10, bcet=1, scheduling_parameter=3))
    T3 = r.bind_task(model.Task("T3", wcet=7,  bcet=1, scheduling_parameter=5))
    T4 = r.bind_task(model.Task("T4", wcet=5,  bcet=1, scheduling_parameter=3))
    Ts1= r.bind_task(model.Task("Ts1",wcet=8,  bcet=1, scheduling_parameter=5))

    T1.in_event_model = model.PJdEventModel(P=25)
    T1.in_typical_event_model = T1.in_event_model
    T1.in_overload_event_model = None
    T2.in_event_model = model.PJdEventModel(P=45)
    T2.in_typical_event_model = T2.in_event_model
    T2.in_overload_event_model = None
    T3.in_event_model = model.PJdEventModel(P=30)
    T3.in_typical_event_model = T3.in_event_model
    T3.in_overload_event_model = None
    T4.in_event_model = model.PJdEventModel(P=25)
    T4.in_typical_event_model = T4.in_event_model
    T4.in_overload_event_model = None
    T4.D = 25
    Ts1.in_event_model= model.PJdEventModel(P=350)
    Ts1.in_typical_event_model = None
    Ts1.in_overload_event_model = Ts1.in_event_model

    task_results = analysis.analyze_system(s)

    for r in sorted(s.resources, key=str):
        for t in sorted(r.tasks, key=str):
            print t.name, task_results[t].wcrt

    t_analyzed = T4
    sporadic = [Ts1]
    start = time.time()
    lp = LP.Solve(s, sporadic, t_analyzed, 10)
    print lp.get_dmm()
    lp = LP.Solve(s, sporadic, t_analyzed, 100)
    print lp.get_dmm()
    lp = LP.Solve(s, sporadic, t_analyzed, 1000)
    print lp.get_dmm()
    end = time.time()
    print"execution time = ", end - start

# The results should be as follows:
# T1 30
# T2 48
# T3 31
# T4 30
# Ts1 34
# dmm (10) = 1
# dmm (100) = 8
# dmm (1000) = 72
# execution time =  x.xx (it depends!)

if __name__ == "__main__":
    test()