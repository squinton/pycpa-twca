"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
This module contains the classes and functions needed to compute the Deadline Miss Model DMM
                                                                for Wighted Round-Robin as defined in[Hammadeh:ETFA2018]
"""
from __future__ import division


from pyCPA_TWCA import schedulers
from pyCPA_TWCA import LP
import sys
import math

epsilon = 0.0001


class SchedulabilityLP(LP.SchedulabilityLP):

    def __init__(self, para):

        super(SchedulabilityLP, self).__init__(para=para)
        self.__WRR__(para=para)

    def __WRR__(self, para):
        """
        :param para:
        :return:
        """
        # add a variable for every sporadic task
        for o in sorted(para.omega, key=lambda task: task.name):
            self.lp_.variables.add(obj=[1.0], lb=[0.0], ub=[1.0], names=[o.name], types="B")

        # add a set of constraints for every activation l in [0,Ni]
        for l in xrange(0, para.Ni):
            row_id = self.lp_.linear_constraints.get_num()
            rh_l = sum(para.wl_wrr[l]) - para.Lambda_wrr[l] + para.Gamma_wrr[l] + epsilon
            self.lp_.linear_constraints.add(senses='G', rhs=[rh_l], names=['t' + str(l)])
            # set the coefficients
            i = 0
            for o in sorted(para.omega, key=lambda task: task.name):
                col_id = self.lp_.variables.get_indices(o.name)
                self.lp_.linear_constraints.set_coefficients(row_id, col_id, para.wl_wrr[l][i])
                i += 1


class Init(LP.Init):
    def __init__(self, system, sporadic, task, k):
        self.system = system
        self.task = task
        self.k = k
        self.Ni = 0
        self.RT = list()
        self.omega = set()
        self.task_set = list()
        self.Lambda_wrr = list()  # wighted round-robin
        self.Gamma_wrr = list()  # wighted round-robin
        self.wl_wrr = list()  # wighted round-robin

        # compute Ni
        self.compute_Ni(task=task)

        r = task.resource
        # print r.scheduler
        if isinstance(r.scheduler, schedulers.RoundRobinScheduler):
            self.compute_omega_wrr(sporadic=sporadic, task=task, k=k)
            self.compute_lambda_wrr(task=task)
            self.compute_gamma_wrr(task=task)
            self.compute_wl_wrr(sporadic=sporadic, task=task)

        else:
            raise Exception("Non supported scheduler! Exit the LP")

        # store the name of sporadic tasks
        for o in self.omega:
            self.task_set.append(o.name)

            # self.print_init()

    # Wighted Round-Robin

    def compute_omega_wrr(self, sporadic, task, k):
        """
        :param sporadic:
        :param task:
        :param k:
        :return:
        """
        # compute the relevant time window Delta_T_i(k) = BW_i + delta_i^+(k) + wcrt_i - theta_i_last
        scheduling_horizon = schedulers.RoundRobinScheduler.s_horizon(task=task)
        # print "dela_plus",task.in_event_model.delta_plus(k)
        Delta_T_k = scheduling_horizon + task.in_event_model.delta_plus(k) + task.analysis_results.wcrt

        # compute the objective function coefficient Omega_j^k for each t_j with higher priority than t_analyzed
        # We have to sort the tasks according to priority (scheduling_parameter) to have consequential matrices!
        for t in sorted(sporadic, key=lambda task__: task__.name):
            ts = LP.Task()
            ts.name = t.name
            ts.omega = t.in_overload_event_model.eta_plus(Delta_T_k)
            assert isinstance(ts.omega, int), "Weird! eta_plus function always returns integers"
            self.omega.add(ts)

    def compute_lambda_wrr(self, task):
        """
        :param task:
        :return:
        """
        rts = self.RT
        for l in xrange(0, len(rts)):
            x = rts[l] - task.D
            if x > 0:
                self.Lambda_wrr.append(x)
        for lam in self.Lambda_wrr:
            assert lam > 0, "Negative Values in Lambda!"
        # print "Lambda", self.Lambda_wrr
        assert len(self.Lambda_wrr) == self.Ni, "size(Lambda) =/= N number of deadline misses"

    def compute_gamma_wrr(self, task):
        """
        :param task:
        :return:
        """
        rts = self.RT
        for l in xrange(1, len(rts)+1):
            if rts[l-1] > task.D:
                gamma_tmp = 0
                for tj in task.resource.tasks:
                    if tj is not task:
                        # eta_B_l = min(int(math.ceil(float(l) * task.wcet / task.scheduling_parameter)) *
                        #               min(ti.scheduling_parameter, ti.wcet), ti.in_event_model.eta_plus(
                        #     task.analysis_results.busy_times[l]) * ti.wcet) What if theta > Ci?
                        eta_B_l = min(int(math.ceil(float(l) * task.wcet / task.scheduling_parameter)) * tj.scheduling_parameter,
                                      tj.in_event_model.eta_plus(task.analysis_results.busy_times[l]) * tj.wcet)
                        eta_D_l = min(int(math.ceil(float(l) * task.wcet / task.scheduling_parameter)) * tj.scheduling_parameter,
                                      int(math.ceil((tj.in_event_model.eta_plus(task.in_event_model.delta_min(l) + task.D)
                                                     * tj.wcet) / tj.scheduling_parameter)) * tj.scheduling_parameter)
                        gamma_tmp += max(eta_B_l - eta_D_l, 0)

                assert gamma_tmp >= 0, (task.name, gamma_tmp)
                self.Gamma_wrr.append(gamma_tmp)
            else:
                pass
        # print "Gamma",self.Gamma_wrr
        assert len(self.Lambda_wrr) == len(self.Gamma_wrr), "Lambda =/= Gamma"

    def compute_wl_wrr(self, sporadic, task):
        """
        :param sporadic:
        :param task:
        :return:
        """
        rts = self.RT
        # print "len(sporadic) at workload", len(sporadic)
        for l in xrange(1, len(rts)+1):
            wl_l = []
            if rts[l-1] > task.D:
                for tj in sorted(sporadic, key=lambda ti: ti.name):
                    if tj is not task:
                        wl_l.append(min(int(math.ceil(float(l) * task.wcet / task.scheduling_parameter)) * tj.scheduling_parameter,
                                    tj.in_overload_event_model.eta_plus(task.in_event_model.delta_min(l) + task.D) * tj.wcet))
                        assert wl_l[-1] >= 0, "Negative workload!"
                    else:
                        wl_l.append(task.in_overload_event_model.eta_plus(task.in_event_model.delta_min(l)) * task.wcet)
                self.wl_wrr.append(wl_l)


class Solve(LP.Solve):
    def __init__(self, system, sporadic, task, k):
        """

        :param system:
        :param sporadic:
        :param task:
        :param k:
        :return:
        """
        para = Init(system, sporadic, task, k)
        # print "Ni= ", para.Ni
        if k <= task.in_event_model.eta_plus(task.analysis_results.busy_times[-1]):
            self.dmm = min(para.Ni, k)
            return
        if len(sporadic) == 1:
            self.dmm = min(para.Ni * list(para.omega)[-1].omega, k)
            return
        mainlp = LP.MainLP(para=para)
        schedulabilitylp = SchedulabilityLP(para=para)
        alg = LP.MainAlgorithm(para=para, mainlp=mainlp, schedulabilitylp=schedulabilitylp)
        self.dmm = min(alg.get_dmm(), k)

        # print ("dmm (%d) = %d") % (k, self.dmm)
        sys.stdout.flush()



