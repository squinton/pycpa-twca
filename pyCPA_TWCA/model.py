"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH
         - Sophie QUINTON

Description
-----------

model is the Typical model which is defined in [Quinton,Hanke2012]_.
This model is an overriding of "model.py" in pycpa to provide the functions
 and the attributes needed to achieve the typical model analysis and compute
 the err function.

"""

from pycpa import model as m
from pycpa import util


class ConstraintsManager(m.ConstraintsManager):
    """ 
    """

    def __init__(self):
        
        # Here you can override class ConstraintsManager
        
        super(ConstraintsManager,self).__init__()
        
class EventModel (m.EventModel):
    """ 
    """

    def __init__(self, name='min', cache=None):
        
        # Here you can override class EventModel
        
        super(EventModel,self).__init__(name,cache)
        
class PJdEventModel (m.PJdEventModel):
    """ 
    """

    def __init__(self, P=0, J=0, dmin=0, name='min', cache=None):
        
        # Here you can override class PJdEventModel
        
        super(PJdEventModel,self).__init__(P,J,dmin,name,cache)
        
class CTEventModel (m.CTEventModel):
    """ 
    """
    def __init__(self, c, T, dmin=1, name='min', cache=None):
        
        # Here you can override class CTEventModel
        
        super(CTEventModel,self).__init__(c,T,dmin,name,cache)
        
class LimitedDeltaEventModel(m.LimitedDeltaEventModel):
    """ 
    """
    def __init__(self,
            limited_delta_min_func=None,
            limited_delta_plus_func=None,
            limit_q_min=float('inf'),
            limit_q_plus=float('inf'),
            min_additive=util.recursive_min_additive,
            max_additive=util.recursive_max_additive,
            name='min', cache=None):
        
        # Here you can override class LimitedDeltaEventModel
        
        super(LimitedDeltaEventModel,self).__init__(limited_delta_min_func,limited_delta_plus_func,
                                                    limit_q_min,limit_q_plus,
                                                    min_additive,max_additive,
                                                    name,cache)
        
class TraceEventModel (m.TraceEventModel):
    """
    """
    def __init__(self, trace_points=[], min_sample_size=20,
                 min_additive=util.recursive_min_additive,
                 max_additive=util.recursive_max_additive,
                 name='min'):
        
        # Here you can override class TraceEventModel
        
        super(TraceEventModel,self).__init__(trace_points, min_sample_size,
                 min_additive,max_additive,
                 name)
 
class Junction(m.Junction):
    """
    """
    def __init__(self, name="unknown", mode='and'):
         
        # Here you can override class Junction
         
        super(Junction,self).__init__(name,mode)
    
class Task(m.Task):
    """
        Overriding of
    """
    
    def __init__(self, name, *args, **kwargs):
        
        ## Typical-case event model activating the task
        self.in_typical_event_model = None

        ## Overload event model activating the task
        self.in_overload_event_model = None
        
        ## Deadline
        self.D = 0

        
        super( Task, self ).__init__(name, *args, **kwargs)

class Resource(m.Resource):
    """
    """
    def __init__(self, name=None, scheduler=None, **kwargs):
        
        # Here you can override class Resource
        
        super(Resource,self).__init__(name,scheduler,**kwargs)

class Mutex( m.Mutex, object):
    """
    """
    def __init__(self, name=None):
        
        # Here you can override class Mutex
        
        super(Mutex,self).__init__(name)

class Path(m.Path , object):
    """
    """
    def __init__(self, name, tasks=None):
        
        # Here you can override class Path
        
        super(Path,self).__init__(name,tasks)

class System( m.System, object):
    """
    """
    def __init__(self, name=''):
        
        # Here you can override class System

        super(System, self).__init__(name)

    def unbind_path(self,path):
        if path in self.paths:
            self.paths.remove(path)
        else:
            print "no path found!"