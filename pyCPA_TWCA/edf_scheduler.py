"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| See LICENSE file for copyright and license details.

:Author:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------

This module is an implementaion of the EDF scheduler as in Spuri:1996

"""

from __future__ import division


from pycpa import analysis
import copy


class EDFScheduler(analysis.Scheduler):
    @staticmethod
    def test_schedulablity(resource):
        """
        It is defined as static method to be called
        :param resource:
        :return:
        """
        L = EDFScheduler.s_horizon_synch(resource)
        deadlines = list()
        for tj in resource.tasks:
            mu = 1
            while tj.in_event_model.delta_min(mu) + tj.scheduling_parameter < L:
                deadlines.append(tj.in_event_model.delta_min(mu) + tj.scheduling_parameter)
                mu += 1
        D = sorted(list(set(deadlines)))

        dmin = min(t_.scheduling_parameter for t_ in resource.tasks)

        t = max(D)
        dbf = 0
        for task in resource.tasks:
            dbf += task.in_event_model.eta_plus_closed(t-task.scheduling_parameter)*task.wcet
        while (dbf <= t) and (dbf > dmin):
            if dbf < t:
                t = dbf
            else:
                while D[-1] >= t:
                    del D[-1]
                t = max(D)
            dbf = 0
            for task in resource.tasks:
                dbf += task.in_event_model.eta_plus_closed(t-task.scheduling_parameter)*task.wcet
        if dbf < dmin:
            return True
        else:
            return False


    @staticmethod
    def s_horizon_synch(resource):
        """
        It is defined as static method to be called
        :param resource:
        :return:
        """
        U = 0
        L = 0
        # U = (99*task.wcet)/task.in_event_model.delta_min(100)
        # print task.name,(99*task.wcet)/task.in_event_model.delta_min(100), " U= ", U
        # L = task.wcet
        # for r in sorted(system.resources,key=str):
        for tj in sorted(resource.tasks, key=str):
            U += (99*tj.wcet)/tj.in_event_model.delta_min(100)
            # print tj.name,(99*tj.wcet)/tj.in_event_model.delta_min(100), " U= ", U
            L += tj.wcet
        assert U <= 1, "Not schedulable, U > 1: U = "+str(U)

        L_new = 0  # task.in_event_model.eta_plus(L) * task.wcet
        # for r in sorted(system.resources,key=str):
        for tj in sorted(resource.tasks, key=str):
            L_new = L_new + tj.in_event_model.eta_plus(L) * tj.wcet

        assert L_new >= L, "Impossible!"

        # print "Lnew=", L,L_new
        while L < L_new:
            L = L_new
            L_new = 0  # task.in_event_model.eta_plus(L) * task.wcet
            for tj in sorted(resource.tasks, key=str):
                L_new = L_new + tj.in_event_model.eta_plus(L) * tj.wcet
            # print "Lnew=", L,L_new
        # print "Synchronous busy period: L=", L_new
        return L_new

    def s_horizon(self, task, a, details=None):
        """
        :param task:
        :param a:
        :param details:
        :return:
        """
        # print task
        # print a

        # the first instance of task i:
        s0 = a - task.in_event_model.delta_min(task.in_event_model.eta_plus_closed(a))  # s_i(a) = a - \floor(a/T_i).T_i
        # initial value of L
        L = 0
        for tj in task.get_resource_interferers():  # task.resource.tasks:
            # if tj.scheduling_parameter <= a + task.scheduling_parameter and tj != task:
            L += tj.wcet
        if s0 == 0:
            L += task.wcet
        # print T[i]
        # print "s0=",s0
        # compute l_i = lambda_i(a,t)
        if L > s0:
            l_i = task.in_event_model.eta_plus(L - s0) * task.wcet
        else:
            l_i = 0

        # print "li=",l_i
        W = 0
        # t = L
        for tj in task.get_resource_interferers():
            # if tj != task:
                W = W + tj.in_event_model.eta_plus(L) * tj.wcet
        L_new = W + l_i
        # print "L_new",L_new

        assert L_new >= L, "Impossible!"

        while L < L_new:
            L = L_new
            # L_new = compute_w_a_t(task_set, task, a, L) + l_i
            if L > s0:
                l_i = task.in_event_model.eta_plus(L - s0) * task.wcet
            else:
                l_i = 0
            W = 0
            # t = L
            for tj in task.get_resource_interferers():
                # if tj != task:
                    W = W + tj.in_event_model.eta_plus(L) * tj.wcet
            L_new = W + l_i

        # print "task", task, ":  L(a =", a, ") = ",L_new
        # print "Response time r =", max(C[i], L_new - a)

        return L_new

    def b_plus(self, task, a, details=None):
        """
        :param task:
        :param a:
        :param details:
        :return:
        """
        # print task
        # print a

        # the first instance of task i:
        s0 = a - task.in_event_model.delta_min(task.in_event_model.eta_plus_closed(a))
        assert s0 >= 0
        # initial value of L
        L = 0
        for tj in task.resource.tasks:  # get_resource_interferers():
            if tj.scheduling_parameter <= a + task.scheduling_parameter and tj != task:
                # print tj.name
                L += tj.wcet
            elif tj == task and s0 == 0:
                L += task.wcet
        # print T[i]
        # print "s0=",s0
        # compute l_i = lambda_i(a,t)
        if L > s0:
            l_i = min(task.in_event_model.eta_plus(L - s0), task.in_event_model.eta_plus_closed(a)) * task.wcet
        else:
            l_i = 0

        # print "li=",l_i
        W = 0
        # t = L
        for tj in task.get_resource_interferers():
            # if tj != task:
            if tj.scheduling_parameter <= a + task.scheduling_parameter:
                W = W + min(tj.in_event_model.eta_plus(L), tj.in_event_model.eta_plus_closed(a+task.scheduling_parameter-tj.scheduling_parameter)) * tj.wcet
        L_new = W + l_i
        # print "L_new=",L_new

        assert L_new >= L, "Impossible!"

        while L < L_new:
            L = L_new
            # L_new = compute_w_a_t(task_set, task, a, L) + l_i
            if L > s0:
                l_i = min(task.in_event_model.eta_plus(L - s0), task.in_event_model.eta_plus_closed(a)) * task.wcet
                # print task.name, a,  s0 ,"li", l_i
            else:
                l_i = 0
            W = 0
            # t = L
            for tj in task.get_resource_interferers():
                # if tj != task:
                if tj.scheduling_parameter <= a + task.scheduling_parameter:
                    W = W + min(tj.in_event_model.eta_plus(L), tj.in_event_model.eta_plus_closed(a+task.scheduling_parameter-tj.scheduling_parameter)) * tj.wcet
            L_new = W + l_i

        # print "task", task, ":  L(a =", a, ") = ",L_new
        # print "Response time r =", max(C[i], L_new - a)

        return L_new

    def compute_wcrt(self, task, task_results=None):
        """
        :param task:
        :param task_results:
        :return:
        """
        # print task.name
        R = list()
        Dmiss = list()
        L = self.s_horizon_synch(task.resource)
        alpha = 0  # because s0 = 0
        num_activations = 1
        d__ = 0
        tmp_r = list()
        # print "a = ", 0
        # print "S_alpha=",  L
        while alpha < L:
            num_activations += 1
            # print "alpha=",  alpha
            B_alpha = self.b_plus(task,alpha)
            # print "B_alpha=",  B_alpha
            r_alpha = max(task.wcet, B_alpha - alpha)
            # print "r_alpha=",  r_alpha
            tmp_r.append(r_alpha)
            # print 0 ,0 , alpha, r_alpha
            if r_alpha > task.scheduling_parameter:
                # print 0 ,0 , alpha, r_alpha
                d__ += 1
            # print task.name, alpha, r_alpha
            alpha = task.in_event_model.delta_min(num_activations)

        R.append(max(tmp_r))
        Dmiss.append(d__)

        # compute the deadline candidates
        # print "threshold = ", threshold
        ek_candidates = []
        for tj in task.get_resource_interferers():
            mu = 1
            while tj.in_event_model.delta_min(mu) + tj.scheduling_parameter < L:
                ek_candidates.append(tj.in_event_model.delta_min(mu) + tj.scheduling_parameter)
                mu += 1

        ek_candidates = sorted(list(set(ek_candidates)))
        # print "candidates:",ek_candidates
        ek_candidates_copy = copy.copy(ek_candidates)
        # s0_used = []
        # s0_used.append(0)
        for d in ek_candidates_copy:
            if ek_candidates == []:
                break
            ek = max(ek_candidates)
            ek_candidates.pop(ek_candidates.index(ek))
            # print "ek = ",ek
            if ek < task.scheduling_parameter:
                break
            a = ek - task.scheduling_parameter
            s0 = a - task.in_event_model.delta_min(task.in_event_model.eta_plus_closed(a))
            # print "a=", a
            S_alpha = self.s_horizon(task, a)
            # print "a = ", a
            # print "S_alpha=",  S_alpha
            if S_alpha <= a:
                # print "S_alpha=", S_alpha, s0
                pass
            elif s0 not in s0_used:
                # s0_used.append(s0)
                d__ = 0
                alpha = s0
                tmp_r = []
                num_activations = 1
                while alpha < S_alpha:
                    num_activations += 1
                    # print "alpha=", alpha
                    B_alpha = self.b_plus(task,alpha)
                    # print "B_alpha=", B_alpha
                    # assert B_alpha > alpha, B_alpha
                    r_alpha = max(task.wcet, B_alpha - alpha)
                    # print "r_alpha=", r_alpha
                    tmp_r.append(r_alpha)
                    # print task.name, s0, a, alpha, r_alpha
                    if r_alpha > task.scheduling_parameter:
                         # print "r_alpha =", r_alpha
                         # print s0, a, alpha, r_alpha
                        d__ += 1
                    alpha = s0 + task.in_event_model.delta_min(num_activations)
                R.append(max(tmp_r))
                Dmiss.append(d__)
        # print task.name, max(R), max(Dmiss)
        if task_results:
            task_results[task].wcrt = max(R)
            task_results[task].dmiss = max(Dmiss)
            task_results[task].busy_times = [0, L]  # TODO improve this

        return max(R)
