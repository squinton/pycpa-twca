"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH
         - Sophie QUINTON

Description
-----------

This module contains the functions needed to do the typical worst-case analysis .
Typical worst-case analysis is defined in [Quinton, Hanke2012].


"""

from pycpa import analysis


def analyze_system(system, clean=False, only_dependent_tasks=False, progress_hook=None):
    """
    Typical worst-case response time analysis.
    Tasks which have no typical event model (sporadic tasks) are not part of the typical analysis!
    To do the typical worst-case analysis we first have to do the worst-case analysis.
    """
    # STEP 1: perform the analysis of the worst-case model

    # print("... performing analysis of the worst-case model ...\n")
    worst_case_results = analysis.analyze_system(system)
    # for r in sorted(system.resources, key=str):
        # for t in sorted(r.tasks, key=str):
            # print"%s: WCRT=%f" % (t.name, worst_case_results[t].wcrt )
    ####################################################################

    # STEP 2: perform the analysis of the typical worst-case model

    # print("... performing analysis of the typical worst-case model ...\n")
               
    # keep the worst-case model and unbind discarded tasks (sporadic tasks)
    in_event_model_tmp = dict()
    worst_case_analysis_results_tmp = dict()
    discarded_tasks = dict()
    for r in system.resources:
        #  tasks which have no typical-case model (sporadic tasks) are not part of the typical worst-case analysis
        discarded_tasks[r] = list()
        for t in r.tasks:
            if t.in_typical_event_model == None:
                discarded_tasks[r].append(t)

            else:  # store the safe event model
                in_event_model_tmp[t] = t.in_event_model
                worst_case_analysis_results_tmp[t] = t.analysis_results
                t.in_event_model = t.in_typical_event_model
        #  unbind discarded tasks!
        for t in discarded_tasks[r]:
            t.unbind_resource()

    typical_case_results = analysis.analyze_system(system)
    # for r in sorted(system.resources, key=str):
    #     for t in sorted(r.tasks, key=str):
    #         print"%s: WCRT=%f" % (t.name, typical_case_results[t].wcrt)
    
    #store the TWCRT and restore the worst-case analysis results
    for r in system.resources:
        for t in r.tasks:
            t.analysis_results = worst_case_analysis_results_tmp[t]
            t.analysis_results.twcrt = typical_case_results[t].wcrt
            t.analysis_results.typical_bplus = typical_case_results[t].busy_times[-1]
            worst_case_results[t].twcrt = typical_case_results[t].wcrt
        for t in discarded_tasks[r]:
            t.analysis_results.twcrt = 0
            worst_case_results[t].twcrt = 0
        
    #restore the safe event model
    for r in system.resources:
        for t in r.tasks:
            if t.in_typical_event_model != None:
                t.in_event_model = in_event_model_tmp[t]

                
    # rebind the discarded tasks (sporadic tasks)
    for r in system.resources:
        for t in discarded_tasks[r]:
            r.bind_task(t)   

    return worst_case_results