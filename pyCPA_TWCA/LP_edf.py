"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
This module contains the classes and functions needed to compute the Deadline Miss Model DMM
                                                                as defined in [Hammadeh2018] for EDF.
"""

from __future__ import division


import cplex
import math
from cplex.exceptions import CplexError
from pyCPA_TWCA import edf_scheduler
from pyCPA_TWCA import model
import sys
import warnings

epsilon = 0.0001


class Task(object):
    def __init__(self):
        """
        :return:
        """
        self.name = ""
        self.omega = 0


class ActPoint(object):
    def __init__(self):
        """
        :return:
        """
        self.time = 0
        self.type = ""
        self.name = ""
        self.load = 0


class MainLP(object):
    def __init__(self, para):
        """
        :param para:
        :return:
        """
        # print "MainLP"
        self.lp_ = cplex.Cplex()
        self.lp_.set_log_stream(None)
        self.lp_.set_results_stream(None)
        self.lp_.objective.set_sense(self.lp_.objective.sense.maximize)
        for o in sorted(para.omega, key=lambda task: task.name):
            self.lp_.linear_constraints.add(senses="L", rhs=[o.omega], names=[o.name])

    def get_lp(self):
        """
        :return:
        """
        return self.lp_


class SchedulabilityLP(object):
    def __init__(self, para):
        """
        :param para:
        :return:
        """
        self.lp_ = cplex.Cplex()
        self.lp_.set_log_stream(None)
        self.lp_.set_results_stream(None)
        self.lp_.objective.set_sense(self.lp_.objective.sense.minimize)

        r = para.task.resource
        if isinstance(r.scheduler, edf_scheduler.EDFScheduler):
            self.__EDF__(para=para)
        else:
            raise Exception("Non supported scheduler, check other implementation! Exit the LP")

    def __EDF__(self, para):
        """
        :param para:
        :return:
        """
        # add a variable for every sporadic task
        for o in sorted(para.omega, key=lambda task: task.name):
            self.lp_.variables.add(obj=[1.0], lb=[0.0], ub=[1.0], names=[o.name], types="B")


    def get_lp(self):
        """
        :return:
        """
        return self.lp_


class MainAlgorithm(object):
    def __init__(self, para, mainlp):
        """
        :param para:
        :return:
        """
        # print"Start the LP algorithm"
        self.dmm = para.k  # step 1
        self.Z = list()
        self.c = list()
        self.x_star = 0
        self.y_star = 0
        self.z_star = 0
        self.v = 0
        self.done = False

        self.Z.append(para.task_set)  # step 1
        self.c = para.task_set

        lp1 = mainlp.get_lp()

        while True:  # step 2
            # step 3
            self.solve_lp(lp=lp1, Ni=para.Ni)
            # step 4
            schedulabilitylp = SchedulabilityLP(para=para)
            lp2 = schedulabilitylp.get_lp()
            self.test_schedulability(lp=lp2, para=para)

            if self.v > para.Ni or (para.Ni - epsilon < self.v < para.Ni + epsilon):  # step 5
                self.dmm = self.z_star  # step 6
                break
            else:  # step 7
                self.Z.append(self.c)  # step 8
                if (self.v > 0) and ((para.Ni / self.v) * self.z_star < self.dmm):  # step 9
                    self.dmm = (para.Ni / self.v) * self.z_star

            if self.z_star == self.dmm:  # step 11
                break

            assert len(self.Z) <= 2 ** (len(para.task_set)), "Exceeds the number of iterations!" + str(para.task_set) +\
                                                             str(len(para.task_set))+"\t"+str(2 ** (len(para.task_set)))
        self.done = True

    def get_dmm(self):
        """
        :return:
        """
        while (not self.done):
            pass
        return self.dmm

    def solve_lp(self, lp, Ni):
        """
        Solve LP (19)-(21) and its dual LP (22)-(24)
            Every call of solve_lp means to add new unschedulable combination which is a new variable to the LP

        :param lp:
        :param Ni:
        :return:
        """

        col_id = lp.variables.get_num()  # get number of columns to know where to add the new variable
        lp.variables.add(obj=[Ni], names=['x' + str(col_id)])  # add a new variable (new an schedulable combination)
        for t in sorted(self.c, key=str):
            row_id = lp.linear_constraints.get_indices(t)
            lp.linear_constraints.set_coefficients(row_id, col_id,
                                                   1.0)  # set the coefficient of the new variable in the constraints
        try:
            lp.solve()
        except CplexError, exe:
            print "MainLP", exe
        lp.write('MainLP.LP')
        self.z_star = lp.solution.get_objective_value()
        # print "z=",self.z_star
        self.x_star = lp.solution.get_values()
        # print "x=", self.x_star
        self.y_star = lp.solution.get_dual_values()

    def test_schedulability(self, lp, para):
        """
        It computes v and it generates a new unschedulable combination by solving (25),(18)

        :param lp:
        :param para:
        :return:
        """
        d_min = Init.compute_dmin(para.task)
        # print "d_min=",d_min
        i = 0
        for o in sorted(para.omega, key=lambda task__: task__.name):
            col_id = lp.variables.get_indices(o.name)
            lp.objective.set_linear(col_id, self.y_star[i])
            i += 1

        deadlines = Init.compute_absolute_deadlines(para.task, para.L)
        t = max(deadlines)
        # add the constraint
        row_id = lp.linear_constraints.get_num()
        lp.linear_constraints.add(senses='G', rhs=[epsilon], names=['C'])

        repeat = True
        # It follows QPA, see [Zhang and Burns 2009]
        while repeat:
            # compute rhs
            dbf = 0
            for task in para.non_sporadic:  # it contains only the non-sporadic tasks
                dbf += Init.compute_dbf(task=task, t=t)
            rhs = t - dbf
            lp.linear_constraints.set_rhs('C', rhs)

            # compute coefficients
            row_id = lp.linear_constraints.get_indices('C')
            for task in para.sporadic:  # it contains only the sporadic tasks
                coefs = Init.compute_dbf(task=task, t=t)
                dbf += coefs
                # set coefficients for sporadic tasks
                col_id = lp.variables.get_indices(task.name)
                lp.linear_constraints.set_coefficients(row_id, col_id, coefs)
            try:
                lp.solve()
                self.v = lp.solution.get_objective_value()
            except:
                # print "Schedulability"
                if dbf < t:
                    # print "case1", dbf, t
                    t = dbf
                elif (dbf - epsilon < t < dbf + epsilon) and (dbf > d_min):
                    # print "case2", dbf, t
                    deadlines = Init.compute_absolute_deadlines(para.task, t)
                    t = max(deadlines)
                elif (dbf < d_min) or (dbf - epsilon < d_min < dbf + epsilon):
                    print "Your system is schedulablbe!"
                    repeat = False
                elif dbf > t:
                    print "You should not be here!"
            else:
                repeat = False

            lp.write('SchedulabilityLP.LP')
        # print "v", self.v
        bj = lp.solution.get_values()
        # print "bj=",bj
        comb = list()
        i = 0
        for t in sorted(para.omega, key=lambda task__: task__.name):
            if 1.0 + epsilon > bj[i] > 1.0 - epsilon:
                comb.append(t.name)
            else:
                pass
            i += 1
        self.c = []
        self.c = comb
        # print "c = ", self.c


class Init(object):
    def __init__(self, system, sporadic, task, k):
        """
        :param system:
        :param sporadic:  is a list of sporadic tasks, every element has the type Task see pycpa.model
        :param task:
        :param k:
        :return:
        """
        assert len(sporadic) >= 1, "No sporadic tasks!"
        # assert task not in sporadic, "task ti should not be a sporadic task"
        for s in sporadic:
            if isinstance(s, model.Task) and not isinstance(s.wcet, int):
                warnings.warn("You are using float numbers for execution times and that may cause too pessimistic"
                              " results!")
        assert k > 0, "k should be > 0"
        assert isinstance(k, int), "k should be int not float"

        self.system = system
        self.sporadic = sporadic
        self.non_sporadic = list()
        self.task = task
        self.L = 0
        self.k = k
        self.Ni = 0
        self.omega = set()
        self.bigM = 0
        self.task_set = list()

        self.non_sporadic.append(task)
        for t in task.get_resource_interferers():
            if t not in sporadic:
                self.non_sporadic.append(t)

        # compute Ni
        self.compute_Ni(task=task)

        r = task.resource
        # print r.scheduler
        if isinstance(r.scheduler, edf_scheduler.EDFScheduler):
            # compute L
            self.compute_synch_l(task=task)
            # compute omega
            self.compute_omega_edf(sporadic=sporadic, task=task, k=k)
            # compute bigM
            self.bigM = self.L + 100
        else:
            raise Exception("Non-supported scheduler! Exit the LP")

        # store the name of sporadic tasks
        for o in self.omega:
            self.task_set.append(o.name)

    def compute_synch_l(self, task):
        """
        :param task:
        :return:
        """
        self.L = edf_scheduler.EDFScheduler.s_horizon_synch(task.resource)

    def compute_Ni(self, task):
        """
        :param task:
        :return:
        """
        N = task.analysis_results.dmiss

        assert N > 0, "task is schedulable, no thing to do!" + task.name
        self.Ni = N

    @staticmethod
    def compute_absolute_deadlines(task, t):
        """
        It is defined as static method to be called
        :param task:
        :param t:
        :return:
        """
        deadlines = list()
        for tj in task.resource.tasks:
            mu = 1
            while tj.in_event_model.delta_min(mu) + tj.scheduling_parameter < t:
                deadlines.append(tj.in_event_model.delta_min(mu) + tj.scheduling_parameter)
                mu += 1

        return sorted(list(set(deadlines)))

    def compute_omega_edf(self, sporadic, task, k):
        """
        :param sporadic:
        :param task:
        :param k:
        :return:
        """
        # compute the relevant time window Delta_T_i(k) = L + delta_i^+(k) + D_i as in the article "General TWCA"
        Delta_T_k = self.L + task.in_event_model.delta_plus(k) + task.scheduling_parameter

        # compute the objective function coefficient Omega_j^k for each t_j
        for t in sorted(sporadic, key=lambda task__: task__.name):
            ts = Task()
            ts.name = t.name
            # ts.omega = 1 + t.in_overload_event_model.eta_min(int(Delta_T_k - t.scheduling_parameter))
            ts.omega = t.in_overload_event_model.eta_plus_closed(int(Delta_T_k - t.scheduling_parameter))
            assert isinstance(ts.omega, int), "Weird! eta_plus function always returns integers"
            self.omega.add(ts)
            # print "omega = ",ts.omega

    @staticmethod
    def compute_dbf(task, t):
        """
        It is defined as static method to be called
        :param task:
        :param t:
        :return:
        """
        # compute the demand bound function
        # return 1 + task.in_event_model.eta_min(t-task.scheduling_parameter)*task.wcet
        return task.in_event_model.eta_plus_closed(t-task.scheduling_parameter)*task.wcet

    @staticmethod
    def compute_dmin(task):
        """
        It is defined as static method to be called
        :param task:
        :return:
        """
        return min(t.scheduling_parameter for t in task.resource.tasks)


class Solve(object):
    def __init__(self, system, sporadic, task, k):
        """
        :param system:
        :param sporadic:
        :param task:
        :param k:
        :return:
        """
        if task in sporadic:
            Ni = task.analysis_results.dmiss
            L = edf_scheduler.EDFScheduler.s_horizon_synch(task.resource)
            self.dmm = Ni * math.ceil(k/task.in_event_model.eta_plus(L))
            return

        para = Init(system=system, sporadic=sporadic, task=task, k=k)
        # print k, para.L, task.in_event_model.eta_plus(para.L)
        if k <= task.in_event_model.eta_plus(para.L):
            self.dmm = min(para.Ni, k)
            return
        if len(sporadic) == 1:
            self.dmm = min(para.Ni * list(para.omega)[-1].omega, k)
            return

        mainlp = MainLP(para=para)
        alg = MainAlgorithm(para=para, mainlp=mainlp)
        self.dmm = min(alg.get_dmm(), k)
        sys.stdout.flush()

    def get_dmm(self):
        """
        :return: dmm the computed deadline miss model
        """
        return self.dmm
