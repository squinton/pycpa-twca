"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH

Description
-----------
This module contains the classes and functions needed to compute the Deadline Miss Model DMM
                                                                as defined in [Xu,Hammadeh:ECRTS2015].
This module has originally written in C by Alexnder Kroeller and Wenbo Xu, This is a cover in python to 
be compatible with pyCPa.
"""
from __future__ import division


import cplex
from cplex.exceptions import CplexError
from pyCPA_TWCA import schedulers
from pyCPA_TWCA import model
import sys
import warnings

epsilon = 0.0001


class Task(object):
    def __init__(self):
        """
        :return:
        """
        self.name = ""
        self.omega = 0


class ActPoint(object):
    def __init__(self):
        """
        :return:
        """
        self.time = 0
        self.type = ""
        self.name = ""
        self.load = 0


class MainLP(object):
    def __init__(self, para):
        """
        :param para:
        :return:
        """
        self.lp_ = cplex.Cplex()
        self.lp_.set_log_stream(None)
        self.lp_.set_results_stream(None)
        self.lp_.objective.set_sense(self.lp_.objective.sense.maximize)
        for o in sorted(para.omega, key=lambda task: task.name):
            self.lp_.linear_constraints.add(senses="L", rhs=[o.omega], names=[o.name])

    def get_lp(self):
        """
        :return:
        """
        return self.lp_


class SchedulabilityLP(object):
    def __init__(self, para):
        """
        :param para:
        :return:
        """
        self.lp_ = cplex.Cplex()
        self.lp_.set_log_stream(None)
        self.lp_.set_results_stream(None)
        self.lp_.objective.set_sense(self.lp_.objective.sense.minimize)

        r = para.task.resource
        if isinstance(r.scheduler, schedulers.SPPScheduler):
            self.__SPP__(para=para)
        elif isinstance(r.scheduler, schedulers.SPNPScheduler):
            self.__SPNP__(para=para)
        elif isinstance(r.scheduler, schedulers.FIFOScheduler):
            self.__FIFO__(para=para)
        # else:
            # raise Exception("Non supported scheduler! Exit the LP")

    def __SPP__(self, para):
        """
        Static Priority Preemptive scheduling policy (it is called also fixed priority preemptive  FPP)
        :param para:
        :return:
        """
        # add a variable for every sporadic task
        for o in sorted(para.omega, key=lambda task: task.name):
            # print o.name
            self.lp_.variables.add(obj=[1.0], lb=[0.0], ub=[1.0], names=[o.name], types="B")
        # add Ni auxiliary variables. These variables represent the "there exist" in equation 18
        for l_ in xrange(0, para.Ni):
            self.lp_.variables.add(obj=[0.0], lb=[0.0], ub=[1.0], names=['l' + str(l_)], types="B")
        # add a set of constraints for every activation l in [0,Ni]
        # number of constraints for every l = number of time points P_i^l
        for l in xrange(1, para.Ni + 1):
            idx = 0
            for rho in para.remain[l]:
                # idx = para.remain[l].index(rho)
                tp = para.time_points[l][idx]
                idx += 1
                row_id = self.lp_.linear_constraints.get_num()
                sum_wl = 0
                for s in para.wl[tp]:
                    sum_wl += para.wl[tp][s]
                self.lp_.linear_constraints.add(senses='G', rhs=[sum_wl - rho + epsilon], names=['t' + str(tp)])
                # set the coefficients
                i = 0
                for o in sorted(para.omega, key=lambda task: task.name):
                    # print o.name
                    col_id = self.lp_.variables.get_indices(o.name)
                    self.lp_.linear_constraints.set_coefficients(row_id, col_id, para.wl[tp][o.name])
                    i += 1
                for l_ in xrange(0, para.Ni):
                    col_id = self.lp_.variables.get_indices('l' + str(l_))
                    if col_id == i + l - 1:
                        self.lp_.linear_constraints.set_coefficients(row_id, col_id, para.bigM)
                    else:
                        self.lp_.linear_constraints.set_coefficients(row_id, col_id, 0)

        # add a constraint for the auxiliary variables
        row_id = self.lp_.linear_constraints.get_num()
        self.lp_.linear_constraints.add(senses='L', rhs=[para.Ni - 1], names=['aux'])
        for o in sorted(para.omega, key=lambda task: task.name):
            # print o.name
            col_id = self.lp_.variables.get_indices(o.name)
            self.lp_.linear_constraints.set_coefficients(row_id, col_id, 0)
        for l__ in xrange(0, para.Ni):
            col_id = self.lp_.variables.get_indices('l' + str(l__))
            self.lp_.linear_constraints.set_coefficients(row_id, col_id, 1)

    def __SPNP__(self, para):
        """
        Static Priority Non-preemptive scheduling policy (it is called also fixed priority non-preemptive  FPNP)
        :param para:
        :return:
        """
        # see equation 2.2.12 in Deliverable F4.2 # FIXME, cite Zain's thesis
        # assert 1<0
        self.__SPP__(para=para)

    def __FIFO__(self, para):
        """
        First-In First-Out scheduling policy
        @param para:
        @return:
        """
        # it is applicable in case you consider FIFO as SPNP with equal priorities
        self.__SPNP__(para=para)


    def get_lp(self):
        """
        :return:
        """
        return self.lp_


class MainAlgorithm(object):
    def __init__(self, para, mainlp, schedulabilitylp):
        """
        See the algorithm in [Xu,Hammadeh:ECRTS2015]
        :param para:
        :return:
        """
        # print"Start the LP algorithm"
        self.dmm = para.k  # step 1
        self.Z = list()
        self.c = list()
        self.x_star = 0
        self.y_star = 0
        self.z_star = 0
        self.v = 0
        self.done = False

        self.Z.append(para.task_set)  # step 1
        self.c = para.task_set

        lp1 = mainlp.get_lp()
        lp2 = schedulabilitylp.get_lp()

        while True:  # step 2
            # step 3
            self.solve_lp(lp=lp1, Ni=para.Ni)
            # self.z_star = self.z_star * para.Ni
            # step 4
            self.test_schedulability(lp=lp2, para=para)

            if self.v > para.Ni or (para.Ni - epsilon < self.v < para.Ni + epsilon):  # step 5
                self.dmm = self.z_star  # step 6
                break
            else:  # step 7
                self.Z.append(self.c)  # step 8
                if (self.v > 0) and ((para.Ni / self.v) * self.z_star < self.dmm):  # step 9
                    self.dmm = (para.Ni / self.v) * self.z_star

            if self.z_star == self.dmm:  # step 11
                break

            assert len(self.Z) <= 2 ** (len(para.task_set)), "Exceeds the number of iterations!"+str(para.task_set) +\
                                                             str(len(para.task_set))+"\t"+str(2 ** (len(para.task_set)))

        self.done = True

    def get_dmm(self):
        """
        :return:
        """
        while not self.done:
            pass
        return self.dmm

    # this function has been added to return the number of steps needed to compute dmm and compare it with the
    # size of the set of unschedulable combinations.
    # this comparison is used in the thesis [Hammadeh,2018]
    def get_num_steps(self):
        """
        @return:
        """
        while not self.done:
            pass
        return len(self.Z)

    def solve_lp(self, lp, Ni):
        """
        Solve LP (19)-(21) and its dual LP (22)-(24) in [Xu,Hammadeh:ECRTS2015]
            Every call of solve_lp means to add new unschedulable combination which is a new variable to the LP

        :param lp:
        :param Ni:
        :return:
        """

        col_id = lp.variables.get_num()  # get number of columns to know where to add the new variable
        lp.variables.add(obj=[Ni], names=['x' + str(col_id)])  # add a new variable (new unschedulable combination)
        # print "C = ",sorted(self.c, key=str)
        for t in sorted(self.c, key=str):
            row_id = lp.linear_constraints.get_indices(t)
            lp.linear_constraints.set_coefficients(row_id, col_id,
                                                   1.0)  # set the coefficient of the new variable in the constraints
        try:
            lp.solve()
        except CplexError, exe:
            print "MainLP", exe
        lp.write('MainLP.LP')
        self.z_star = lp.solution.get_objective_value()
        # print "z=",self.z_star
        self.x_star = lp.solution.get_values()
        # print "x=", self.x_star
        self.y_star = lp.solution.get_dual_values()
        # print "y=",self.y_star

    def test_schedulability(self, lp, para):
        """
        It computes v and it generates a new unschedulable combination by solving (25),(18)

        :param lp:
        :param para:
        :return:
        """
        # print "test sched"
        i = 0
        for o in sorted(para.omega, key=lambda task: task.name):
            # print o.name
            col_id = lp.variables.get_indices(o.name)
            lp.objective.set_linear(col_id, self.y_star[i])
            i += 1
        try:
            lp.solve()
            lp.write('SchedulabilityLP.LP')
            self.v = lp.solution.get_objective_value()
            # print "v", self.v
            bj = lp.solution.get_values()
            # print "bj=",bj
            comb = list()
            i = 0
            for t in sorted(para.omega, key=lambda task: task.name):
                # print o.name
                if 1.0 + epsilon > bj[i] > 1.0 - epsilon:
                    comb.append(t.name)
                else:
                    pass
                i += 1
            self.c = []
            self.c = comb
        except CplexError, exe:
            print "Schedulability", exe

        # print "c = ", self.c
        # lp.write('SchedulabilityLP.LP')


class Init(object):
    def __init__(self, system, sporadic, task, k):
        """
        :param system:
        :param sporadic:  is a list of sporadic tasks, every element has the type Task see pycpa.model
        :param task:
        :param k:
        :return:
        """
        assert len(sporadic) >= 1, "No sporadic tasks!"
        assert task not in sporadic, "task ti should not be a sporadic task"
        for s in sporadic:
            if isinstance(s, model.Task) and not isinstance(s.wcet, int):
                warnings.warn("You are using float numbers for execution times and that may cause too pessimistic"
                              " results!")
        assert k > 0, "k should be > 0"
        assert isinstance(k, int), "k should be int not float"
        self.system = system
        self.task = task
        self.k = k
        self.typical = list()
        self.interference_set = list()
        self.Ni = 0
        self.RT = list()
        self.omega = set()
        self.act_points = set()
        self.time_points = dict()
        self.wl = dict()
        self.remain = dict()
        self.bigM = 0
        self.task_set = list()

        # compute interference set
        self.compute_interference(task=task)
        # compute Ni
        self.compute_Ni(task=task)

        r = task.resource
        # print r.scheduler
        if isinstance(r.scheduler, schedulers.SPPScheduler):
            # compute activation points
            self.compute_act_points(system=system, sporadic=sporadic, task=task)
            # compute Omega_j
            self.compute_omega_spp(sporadic, task, k)
            # compute the time points
            self.compute_tp_spp(task=task)
            # compute wl
            self.compute_wl_spp(sporadic=sporadic)
            # compute remain
            self.compute_remain_spp(task=task)
        elif isinstance(r.scheduler, schedulers.SPNPScheduler):
            # compute activation points
            self.compute_act_points(system=system, sporadic=sporadic, task=task)
            # compute Omega_j
            self.compute_omega_spnp(sporadic=sporadic, task=task, k=k)
            # compute the time points
            self.compute_tp_spnp(task=task)
            # compute wl
            self.compute_wl_spnp(sporadic=sporadic)
            # compute remain
            self.compute_remain_spnp(task=task)

        else:
            raise Exception("Non supported scheduler! Exit the LP")

        # compute bigM
        for t in self.wl:
            for s in self.wl[t]:
                self.bigM += self.wl[t][s]
        self.bigM += 100
        # store the name of sporadic tasks
        for o in self.omega:
            self.task_set.append(o.name)

        for t in task.resource.tasks:
            if t not in sporadic:
                self.typical.append(t)

            # self.print_init()

    # common methods
    def compute_interference(self, task):

        for t in task.resource.tasks:
            if t != task and t.scheduling_parameter <= task.scheduling_parameter:
                self.interference_set.append(t)

    def compute_Ni(self, task):
        """
        :param task:
        :return:
        """
        # compute the response times of t_i in the longest busy-window BW^+_i
        rts = list()
        for i in range(1, len(task.analysis_results.busy_times)):
            rts.append(task.analysis_results.busy_times[i] - task.in_event_model.delta_min(i))
        self.RT = rts
        # print "rts=",rts
        # compute the number of activations of t_i in the longest busy window which miss their deadline
        N = 0
        for l in range(0, len(rts)):
            if rts[l] > task.D:
                N += 1
        # assert N > 0, "task is schedulable, no thing to do!"+task.name
        self.Ni = N

    def compute_act_points(self, sporadic, task):
        """
        :param system:
        :param sporadic:
        :param task:
        :return:
        """
        rts = self.RT

        # for r in system.resources:
        r = task.resource
        # if not isinstance(r.scheduler, schedulers.SPPSchedulerSyncOffset):
        # SPP or SPNP
        for t in sorted(r.tasks, key=str):
            if t in self.interference_set:
                n_act = t.in_event_model.eta_plus(task.analysis_results.busy_times[-1])
                if t in sporadic:  # capture the type of the task
                    t_type = 'S'
                else:
                    t_type = 'P'
                for i in xrange(0, n_act):
                    act = ActPoint()
                    act.time = t.in_event_model.delta_min(i + 1)
                    act.type = t_type
                    act.name = t.name
                    act.load = t.wcet
                    if not isinstance(act.time, int):
                        warnings.warn("you are using float numbers!")
                    self.act_points.add(act)
            # capture the deadlines / deadline -Ci
        # SPP
        if not isinstance(r.scheduler, schedulers.SPNPScheduler):
            d = 1
            for n in xrange(0, len(rts)):
                if rts[n] > task.D:
                    act_D = ActPoint()
                    act_D.time = task.in_event_model.delta_min(n + 1) + task.D
                    act_D.type = 'D'
                    act_D.name = task.name
                    act_D.load = 0
                    if not isinstance(act_D.time, int):
                        warnings.warn("you are using float numbers!")
                    self.act_points.add(act_D)
                    d += 1
            assert d == self.Ni + 1
        # SPNP
        elif isinstance(r.scheduler, schedulers.SPNPScheduler):
            d = 1
            for n in xrange(0, len(rts)):
                if rts[n] > task.D:
                    act_D = ActPoint()
                    act_D.time = task.in_event_model.delta_min(n + 1) + task.D - task.wcet
                    act_D.type = 'D'
                    act_D.name = task.name
                    act_D.load = 0
                    if not isinstance(act_D.time, int):
                        warnings.warn("you are using float numbers!")
                    self.act_points.add(act_D)
                    d += 1
            assert d == self.Ni + 1

    # SPP
    def compute_omega_spp(self, sporadic, task, k):
        """
        Compute the maximum number of sporadic overload activations in the maximum window of k-sequence.
        :param sporadic:
        :param task:
        :param k:
        :return:
        """
        # compute the relevant time window Delta_T_i(k) = BW_i + delta_i^+(k) + wcrt_i as in the EMSOFT submission ---
        # q = len(task.analysis_results.busy_times)
        scheduling_horizon = schedulers.SPPScheduler.s_horizon(task)
        assert(scheduling_horizon == task.analysis_results.busy_times[-1]), "Some thing wrong in the computation of " \
                                                                            "scheduling horizon"
        Delta_T_k = scheduling_horizon + task.in_event_model.delta_plus(k) + task.analysis_results.wcrt
        # print "delta = ", Delta_T_k
        # compute the objective function coefficient Omega_j^k for each t_j with higher priority than t_analyzed
        # We have to sort the tasks according to priority (scheduling_parameter) to have consequential matrixes!
        for t in sorted(sporadic, key=lambda task__: task__.name):
            assert t in self.interference_set, "You consider a lower priority sporadic task"
            ts = Task()
            ts.name = t.name
            ts.omega = t.in_overload_event_model.eta_plus(Delta_T_k)
            assert isinstance(ts.omega, int), "Weird! eta_plus function always returns integers"
            self.omega.add(ts)
            # print "omega = ",ts.omega

    def compute_tp_spp(self, task):
        """
        Compute the set of time points P^l_i as in (17)

        :param task:
        :return:
        """
        rts = self.RT
        l_ = list()
        for act in self.act_points:
            l_.append(act.time)
        s_ = set(l_)  # Get unique values from activation points
        l = 0
        for n in xrange(0, len(rts)):
            if rts[n] > task.D:
                l += 1
                l_d = []
                for t in s_:
                    if task.in_event_model.deltamin_func(n+1) < t <= task.in_event_model.deltamin_func(n+1) + task.D:
                        l_d.append(t)
                self.time_points[l] = l_d

    def compute_wl_spp(self, sporadic):
        """
        compute the wl for every sporadic task at every activation point see equation (14)

        :param sporadic:
        :return:
        """
        l_ = list()
        for act in self.act_points:
            l_.append(act.time)
        s_ = set(l_)  # Get unique values from activation points
        for t in s_:
            wl_ = dict()
            for S in sorted(sporadic, key=lambda task__: task__.name):  # you have to sort to have consequential matrixes!
                assert S in self.interference_set, "You consider a lower priority sporadic task"
                if t == 0:
                    # wl_[S.name] = 0 #S.wcet
                    continue
                else:
                    wl_[S.name] = S.in_overload_event_model.eta_plus(t) * S.wcet
            self.wl[t] = wl_

    def compute_remain_spp(self, task):
        """
        compute the remain for every activation of task t_i at every activation point see equation (13)

        :param task:
        :return:
        """
        rts = self.RT
        deadline_miss = 0
        for activation in xrange(0, len(rts)):
            if rts[activation] > task.D:
                deadline_miss += 1
                l_d = []
                for t in self.time_points[deadline_miss]:
                    # if task.in_event_model.deltamin_func(l) < t <= task.in_event_model.deltamin_func(l) + task.D:  # I think it is a wrong condition
                    sum_cj = 0
                    # for tj in sorted(task.resource.tasks, key=str):
                    #     if tj.scheduling_parameter <= task.scheduling_parameter and tj != task: # this has been apdated to include equal priorities
                    for tj in sorted(self.interference_set, key=lambda task__: task__.name):
                        sum_cj += tj.in_event_model.eta_plus(t) * tj.wcet
                    l_d.append(sum_cj + (activation + 1) * task.wcet - t)
                self.remain[deadline_miss] = l_d

    # SPNP
    def compute_omega_spnp(self, sporadic, task, k):
        """
        Compute the maximum number of sporadic overload activations in the maximum window of k-sequence.
        :param sporadic:
        :param task:
        :param k:
        :return:
        """
        #compute QD_i
        qd = task.analysis_results.max_backlog
        # print "qd",qd
        # compute the relevant time window Delta_T_i(k) = BW_i + delta_i^+(k) + QD_i as in the EMSOFT submission ---
        # q = len(task.analysis_results.busy_times)
        scheduling_horizon = schedulers.SPNPScheduler.s_horizon(task)
        Delta_T_k = scheduling_horizon + task.in_event_model.delta_plus(k) + qd
        # print "delta tk",task.analysis_results.busy_times[-1], task.in_event_model.delta_plus(k)
        # compute the objective function coefficient Omega_j^k for each t_j with higher priority than t_analyzed
        # We have to sort the tasks according to priority (scheduling_parameter) to have consequential matrixes!
        for t in sorted(sporadic, key=lambda task__: task__.name):
            assert t in self.interference_set, "You consider a lower priority sporadic task"
            ts = Task()
            ts.name = t.name
            ts.omega = t.in_overload_event_model.eta_plus(Delta_T_k)
            assert isinstance(ts.omega, int), "Weird! eta_plus function always returns integers"
            self.omega.add(ts)

    def compute_tp_spnp(self, task):
        """
        Compute the set of time points P^l_i as in Zain's thesis
        :param task:
        :return:
        """
        rts = self.RT
        l_ = list()
        for act in self.act_points:
            l_.append(act.time)
        s_ = set(l_)  # Get unique values from activation points
        # for l in xrange(1, self.Ni + 1):
        l = 0
        for n in xrange(0, len(rts)):
            if rts[n] > task.D:
                l += 1
                l_d = []
                for t in s_:
                    if task.in_event_model.deltamin_func(n + 1) < t <= task.in_event_model.deltamin_func(
                            n + 1) + task.D - task.wcet:  # I think it is a wrong condition
                        l_d.append(t)
                self.time_points[l] = l_d
        # print "time points", self.time_points

    def compute_wl_spnp(self, sporadic):
        """
        compute the wl for every sporadic task at every activation point see equation (?) in Zain's thesis
        :param sporadic
        :return:
        """
        l_ = list()
        for act in self.act_points:
            l_.append(act.time)
        s_ = set(l_)  # Get unique values from activation points
        for t in s_:
            wl_ = dict()
            for S in sorted(sporadic, key=lambda task__: task__.name):  # you have to sort to have consequential matrices!
                if t == 0:
                    # wl_[S.name] = 0 #S.wcet
                    continue
                else:
                    wl_[S.name] = S.in_overload_event_model.eta_plus(t) * S.wcet
            self.wl[t] = wl_

    def compute_remain_spnp(self, task):
        """
        compute the remain for every activation of task t_i at every activation point see equation (?) in Zain's thesis
        :param task:
        :return:
        """
        max_exe = list()
        for t in task.resource.tasks:
            if t.scheduling_parameter > task.scheduling_parameter:
                max_exe.append(t.wcet)
        if max_exe:
            bi = max(max_exe)
        else:
            bi = 0

        rts = self.RT

        deadline_miss = 0
        for activation in xrange(0, len(rts)):
            if rts[activation] > task.D:
                deadline_miss += 1
                l_d = []
                for t in self.time_points[deadline_miss]:
                    sum_cj = 0
                    for tj in sorted(self.interference_set, key=lambda task__: task__.name):
                        sum_cj += tj.in_event_model.eta_plus(t) * tj.wcet
                    l_d.append(
                        sum_cj + activation * task.wcet + bi - t)  # deadline misses are not necessary consecutive
                assert l_d != []
                self.remain[deadline_miss] = l_d

    # def print_init(self):
    #     print "k = ", self.k
    #     print "Ni = ", self.Ni
    #     print "Omega = "
    #     for o in self.omega:
    #         print o.name, " , ", o.omega
    #     print "act_points = "
    #     for act in self.act_points:
    #         print act.time, " , ", act.type, " , ", act.name, " , ", act.load
    #     print "time_points = ", self.time_points
    #     print "wl = ", self.wl
    #     print "remain = ", self.remain
    #     print "bigM =", self.bigM
    #     print "task_set=", self.task_set


class Solve(object):
    def __init__(self, system, sporadic, task, k):
        """
        :param system:
        :param sporadic:
        :param task:
        :param k:
        :return:
        """
        para = Init(system=system, sporadic=sporadic, task=task, k=k)
        self.Ni = para.Ni
        if k <= task.in_event_model.eta_plus(task.analysis_results.busy_times[-1]):
            self.dmm = min(para.Ni, k)
            self.num_of_steps = 0
            return
        if len(sporadic) == 1:
            self.dmm = min(para.Ni * list(para.omega)[-1].omega, k)
            self.num_of_steps = 0
            return
        mainlp = MainLP(para=para)
        schedulabilitylp = SchedulabilityLP(para=para)
        alg = MainAlgorithm(para=para, mainlp=mainlp, schedulabilitylp=schedulabilitylp)
        self.dmm = min(alg.get_dmm(), k)
        self.num_of_steps = alg.get_num_steps()
        # print task.name
        # print ("dmm (%d) = %d") % (k, self.dmm)
        sys.stdout.flush()

    def get_dmm(self):
        """
        :return: dmm the computed deadline miss model
        """
        return self.dmm

    def get_ni(self):
        """
        @return: Ni the maximum number of deadline misses in one busy window
        """
        return self.Ni

    def get_num_of_steps(self):
        """
        @return:
        """
        return self.num_of_steps
