"""
| Copyright (C) 2014-2018 Zain Alabedin HAJ HAMMADEH, Sophie QUINTON
| TU Braunschweig, Germany
| Inria Grenoble - Rh\^{o}ne-Alpes, France
| All rights reserved.
| This software is made publicly available solely for non-profit and research purposes.
| For any other usage, please contact sophie.quinton@inria.fr and hammadehzain@gmail.com to discuss licensing terms.
| See LICENSE file for copyright and license details.

:Authors:
         - Zain Alabedin HAJ HAMMADEH
Description
-----------

Setup
"""


from setuptools import setup

setup(name='pyCPA_TWCA',
      version='1.0',
      description='pyCPA_TWCA - a pyCPA extension to perform typical worst-case analysis',
      author='Zain A. H. Hammadeh, Sophie Quinton',
      author_email='{hammadeh}@ida.ing.tu-bs,de',
      packages=['pyCPA_TWCA'],
      install_requires=[ 'pycpa'],
      extras_require={}
     )
