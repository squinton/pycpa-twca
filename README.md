pyCPA_TWCA is a python implementation of the analysis presented in [Hammadeh 2019] to compute Deadline Miss Models (DMMs) for independent Weakly-Hard Real-Time (WHRT) tasks in temporarily overloaded systems. pyCPA_TWCA is implemented as a plug-in for pycpa that is an open-source tool to compute real-time guarantees,e.g. WCRT and end-to-end latency.

0. What pyCPA_TWCA is for:
--------------------------
pyCPA_TWCA computes for a given WHRT task WHRT guarantees in the form of a DMM. It supports:
	- Fixed Priority Preemptive FPP,
	- Fixed Priority Non-Preemptive  FPNP,
	- Weighted Round-Robin WRR,
	- Earliest Deadline First EDF Scheduling policies.

1. Installation:
----------------

1.1. Requirements:
------------------
	- Python (>2.6, <3.0).
	- setuptools: to run setup.py. 
	- IBM ILOG CPLEX Optimizer. It is free of charge for academic usage.

1.2. Setting up the prototype:
----------------------------
You can use one of the following ways:
	- Install/copy the prototype into your Python installation.
	
		$ python setup.py install
	
	- Leave the prototype where it is and tell Python to use the module in-place.
	
		$ export PYTHONPATH="/path/to/prototype"
	
	- Also, you can import the prototype as a PyCharm project (or eclipse if you want). This way is the simplest and highly recommended. 


To test it you may want to run the examples which are provided with the prototype.

	$ python /path/to/examples/simple_fpp_lp.py

Or, if you decided to use PyCharm just run simple_fpp_lp.py as a Python application.

2. Content
----------
The package consists of:
	- ReadMe: this file
	- setup.py: a setup file to install the prototype
	- pycpa: python implementation of Compositional Performance Analysis
	- pyCPA_TWCA: python implementation of Typical Worst-Case Analysis for independent tasks
	- examples: various examples to test the prototype

2.1. pyCPA_TWCA:
----------------
pyCPA_TWCA is organized as follows:

	- analysis.py: contains the functions for worst-case and typical worst-case analysis.
	- edf_scheduler.py: EDF scheduler, which is not covered in pycpa.
	- model.py: describes the system model for TWCA. It is an overriding of pycpa.model to cover the TWCA task model.
	- LP.py: defines and solve the LP needed to compute the DMM. Here you can find the LP for FPP, FPNP.
	- LP_wrr.py: defines and solve the LP needed to compute the DMM. Here you can find the LP for WRR.
	- LP_edf.py: defines and solve the LP needed to compute the DMM. Here you can find the LP for EDF.

2.2. examples:
--------------
Various examples can be found in this folder, namely:
	- simple_fpp_lp.py: simple example using FPP.
	- simple_fpnp_lp.py: simple example using FPNP.
	- simple_wrr_lp.py: simple example using WRR.
	- simple_edf_lp.py: simple example using EDF.


